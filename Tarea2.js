document.addEventListener("DOMContentLoaded", function() {
    document.getElementById("formulario").addEventListener('submit', validarFormulario); 
  });
  function validarFormulario(evento) {
    evento.preventDefault();
    var nombre = document.getElementById('Nombre').value;
    if(nombre.length == 0) {
      alert('No has escrito nada en el nombre');
      return;
    }
    var apellido = document.getElementById('Apellido').value;
    if(apellido.length == 0) {
      alert('No has escrito nada en el apellido');
      return;
    }
    var email = document.getElementById('Correo').value;
    if(email.length == 0) {
      alert('No has escrito nada en el email');
      return;
    }
    var domicil = document.getElementById('domicilio').value;
    if(domicil.length == 0) {
      alert('No has escrito nada en el domicilio');
      return;
    }
    
    this.submit();
  }